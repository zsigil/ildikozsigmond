import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

import vueSmoothScroll from 'vue2-smooth-scroll'
Vue.use(vueSmoothScroll)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
  // RenderAfterDocument Event:'render-event'name must correspond to vue-config.js
  mounted () {
    document.dispatchEvent(new Event('render-event'))
  }
}).$mount('#app')
